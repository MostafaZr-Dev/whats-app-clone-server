import roomModel from "../model/room.js";

export const saveMessageToRoom = async (roomId, message) => {
  try {
    const result = await roomModel.update(
      { _id: roomId },
      {
        $push: {
          messages: message,
        },
      }
    );

    return {
      success: true,
      data: result,
    };
  } catch (error) {
    return {
      success: false,
      error,
    };
  }
};
