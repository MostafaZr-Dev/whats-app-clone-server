import jwt from "jsonwebtoken";

export const sign = (data) => {
  return jwt.sign(data, process.env.APP_SECRET);
};

export const verify = (token) => {
  try {
    return jwt.verify(token, process.env.APP_SECRET);
  } catch (error) {
    return false;
  }
};
