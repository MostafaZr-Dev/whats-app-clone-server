import gravatar from "gravatar";

import userModel from "../model/user.js";
import * as hashService from "../services/hashService.js";

export const getUserById = async (userId) => {
  try {
    const result = await userModel.findById(userId);
    if (!result) {
      return {
        success: false,
        data: null,
      };
    }

    return {
      success: true,
      data: result,
    };
  } catch (error) {
    return {
      success: false,
      error,
    };
  }
};

export const authenticate = async (userEmail, userPassword) => {
  try {
    const user = await userModel.findOne({ email: userEmail });

    if (!user) {
      return {
        success: false,
      };
    }

    const passwordCheck = hashService.comparePassword(
      userPassword,
      user.password
    );

    if (!passwordCheck) {
      return {
        success: false,
      };
    }

    return {
      success: true,
      user,
    };
  } catch (error) {
    return {
      success: false,
      error,
    };
  }
};

export const create = async (data) => {
  try {
    const hashedPassword = hashService.hashPassword(data.userPassword);

    const newUser = {
      userName: data.userName,
      email: data.userEmail,
      password: hashedPassword,
    };

    const result = await userModel.create(newUser);

    if (!result) {
      return {
        success: false,
        data: null,
      };
    }

    return {
      success: true,
      data: result,
    };
  } catch (error) {
    return {
      success: false,
      error,
    };
  }
};

export const getGravatarUrl = (email) => {
  return gravatar.url(email, { s: "100", r: "x", d: "retro" }, true);
};
