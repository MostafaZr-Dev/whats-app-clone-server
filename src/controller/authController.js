import * as userService from "../services/userService.js";
import * as tokenService from "../services/tokenService.js";

export const authenticate = async (req, res) => {
  try {
    const { userEmail, userPassword } = req.body;

    const result = await userService.authenticate(userEmail, userPassword);

    if (!result.success) {
      throw new Error("error");
    }

    const { user } = result;

    const token = tokenService.sign({ id: user._id });

    const avatar = userService.getGravatarUrl(user.email);

    res.send({
      success: true,
      user: {
        id: user._id,
        userName: user.userName,
        email: user.email,
        avatar,
      },
      token,
    });
  } catch (error) {
    console.log(error);
    res.status(422).send({ success: false });
  }
};

export const register = async (req, res) => {
  const { userName, userEmail, userPassword } = req.body;

  const result = await userService.create({
    userName,
    userEmail,
    userPassword,
  });

  if (!result.success) {
    return res.status(500).send("");
  }

  res.status(201).send("user created...");
};

export const check = async (req, res) => {
  let token = req.header("Authorization").split(" ").pop();

  const verifyToken = tokenService.verify(token);

  if (!verifyToken) {
    return res.status(422).send("token is not valid!");
  }

  const userId = verifyToken.id;
  const result = await userService.getUserById(userId);
  console.log(result);
  if (!result.success) {
    return res.status(500).send("problem");
  }

  const user = result.data;
  const avatar = userService.getGravatarUrl(user.email);

  res.send({
    success: true,
    user: {
      id: user._id,
      userName: user.userName,
      email: user.email,
      avatar,
    },
  });
};
