import path from "path";
import fs from "fs";

import roomModel from "../model/room.js";
import pusher from "../pusher.js";
import { saveMessageToRoom } from "../services/roomService.js";
import RoomTransform from "../transformer/roomTransform.js";

const __dirname = path.resolve();
const uploadPath = path.join(__dirname, `/public/uploads`);

const roomTransform = new RoomTransform();

export const getRooms = (req, res) => {
  roomModel.find((err, data) => {
    if (err) {
      res.status(500).send(err);
    } else {
      const transformedRoom = roomTransform.collection(data);

      res.status(200).send(transformedRoom);
    }
  });
};

export const getRoomById = async (req, res) => {
  const { roomId } = req.params;

  try {
    const result = await roomModel.findById({ _id: roomId });

    if (!result) {
      throw new Error("nothing..");
    }

    const transformedRoom = roomTransform.transform(result);

    res.send(transformedRoom);
  } catch (error) {
    console.log(error);
    res.status(422).send("room not found");
  }
};

export const createRoom = (req, res) => {
  const roomData = req.body;

  roomModel.create(roomData, (err, data) => {
    if (err) {
      res.status(500).send(err);
    } else {
      pusher.trigger("app", "new-room", {
        id: data._id,
        name: data.name,
        messages: [],
      });
      res.status(201).send(`new room created...`);
    }
  });
};

export const saveMessageRoom = async (req, res) => {
  try {
    const { roomId } = req.params;
    const { id, name, message, timestamp } = req.body;

    const messageData = {
      user: id,
      name,
      message,
      type: "text",
    };
    const result = await saveMessageToRoom(roomId, messageData);

    if (!result.success) {
      throw new Error(result.error);
    }

    const resultRoom = await roomModel.findById({ _id: roomId });

    if (!resultRoom) {
      throw new Error("nothing..");
    }

    const lastMessage = resultRoom.messages.pop();

    pusher.trigger("app", "new-room-message", {
      roomId,
      message: {
        id: lastMessage._id,
        userId: id,
        userName: name,
        message,
        timestamp,
        type: "text",
        url: null,
      },
    });

    res.status(201).send(`new message added...`);
  } catch (error) {
    console.log(error);
    res.status(500).send(error.message);
  }
};

export const deleteRoom = async (req, res) => {
  const { roomId } = req.params;

  try {
    await roomModel.remove({ _id: roomId });

    if (fs.existsSync(`${uploadPath}/images/${roomId}`)) {
      fs.rmdirSync(`${uploadPath}/images/${roomId}`, {
        recursive: true,
        force: true,
      });
    }

    if (fs.existsSync(`${uploadPath}/voices/${roomId}`)) {
      fs.rmdirSync(`${uploadPath}/voices/${roomId}`, {
        recursive: true,
        force: true,
      });
    }

    res.status(200).send({
      success: true,
      roomId,
    });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      success: false,
      err: err.message,
    });
  }
};

export const saveFile = async (req, res) => {
  try {
    const { roomId } = req.params;
    const { id, name, timestamp } = req.body;

    const file = req.file;
    const filename = file.filename;
    const fileType = file.mimetype.split("/").shift();

    let fileUrl = null;
    if (fileType === "audio") {
      fileUrl = `//${req.headers.host}/uploads/voices/${roomId}/${filename}`;
    }

    if (fileType === "image") {
      fileUrl = `//${req.headers.host}/uploads/images/${roomId}/${filename}`;
    }

    const messageData = {
      user: id,
      name,
      message: null,
      type: fileType,
      url: fileUrl,
    };

    const result = await saveMessageToRoom(roomId, messageData);

    if (!result.success) {
      throw new Error(result.error);
    }

    const resultRoom = await roomModel.findById({ _id: roomId });

    if (!resultRoom) {
      throw new Error("nothing..");
    }

    const lastMessage = resultRoom.messages.pop();

    pusher.trigger("app", "new-room-message", {
      roomId,
      message: {
        id: lastMessage._id,
        userId: id,
        userName: name,
        message: null,
        url: fileUrl,
        timestamp,
        type: fileType,
      },
    });

    res.status(201).send(`new message added...`);
  } catch (error) {
    console.log(error);
    res.status(500).send(error.message);
  }
};

export const download = async (req, res) => {
  const { imageUrl } = req.body;
  const { roomId } = req.params;

  const fileName = imageUrl.split("/").pop();

  res.download(`${uploadPath}/images/${roomId}/${fileName}`);
};
