import mongoose from "mongoose";

import pusher from "../../pusher.js";

const MONGO_USER = process.env.MONGO_USER;
const MONGO_PASSWORD = process.env.MONGO_PASSWORD;
const MONGO_DB = process.env.MONGO_DB;
const MONGO_HOST = process.env.MONGO_HOST;

const MONGO_URL = `mongodb://${MONGO_HOST}/${MONGO_DB}`;
const MONGO_ATLAS_URL = `mongodb+srv://${MONGO_USER}:${MONGO_PASSWORD}@cluster0.vme9s.mongodb.net/${MONGO_DB}?retryWrites=true&w=majority`;

mongoose.connect(MONGO_URL, {
  useCreateIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const db = mongoose.connection;

export default () => {
  db.once("open", () => {
    console.log("mongodb connected...");

    // const roomCollection = db.collection("rooms");
    // const roomChangeStream = roomCollection.watch();

    // roomChangeStream.on("change", (change) => {
    //   switch (change.operationType) {
    //     case "insert":
    //       const roomDetails = change.fullDocument;
    //       pusher.trigger("app", "new-room", {
    //         _id: roomDetails._id,
    //         name: roomDetails.name,
    //       });
    //       break;
    //     case "update":
    //       const roomId = change.documentKey._id;
    //       const message = Object.values(
    //         change.updateDescription.updatedFields
    //       ).shift();
    //       pusher.trigger("app", "new-room-message", {
    //         roomId,
    //         message,
    //       });
    //       break;
    //   }
    // });
  });
};
