import express from "express";

import * as roomController from "../controller/roomController.js";
import uploader from "../middleware/multer.js";

const Router = express.Router();

Router.get("/list", roomController.getRooms);

Router.get("/:roomId", roomController.getRoomById);

Router.post("/:roomId/download", roomController.download);

Router.post("/new", roomController.createRoom);

Router.post("/:roomId/new", roomController.saveMessageRoom);

Router.post(
  "/:roomId/upload",
  uploader.single("file"),
  roomController.saveFile
);

Router.delete("/:roomId/delete", roomController.deleteRoom);

export default Router;
