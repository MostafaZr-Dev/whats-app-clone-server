import express from "express";

import * as authController from "../controller/authController.js";

const Router = express.Router();

Router.get("/check", authController.check);
Router.post("/login", authController.authenticate);
Router.post("/register", authController.register);

export default Router;
