class RoomTransform {
  constructor() {
    this.rooms = [];
  }

  transform(item) {
    return {
      id: item._id,
      name: item.name,
      messages: this.messageCollection(item.messages),
    };
  }

  collection(items) {
    return items.map((item) => this.transform(item));
  }

  messageCollection(messages) {
    return messages.map((message) => ({
      id: message._id,
      type: message.type,
      userId: message.user,
      userName: message.name,
      url: message.url,
      timestamp: message.timestamp,
      message: message.message,
    }));
  }
}

export default RoomTransform;
