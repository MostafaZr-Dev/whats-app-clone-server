import express from "express";
import cors from "cors";

import mongoConnection from "./infrastructure/connection/mongoose.js";
import roomRouter from "./router/roomRouter.js";
import authRouter from "./router/authRouter.js";

const app = express();

mongoConnection();

app.use(express.json());
app.use(
  cors({
    origin: "http://localhost:3000",
  })
);
app.use(express.static("public"));

app.use("/api/v1/room", roomRouter);
app.use("/api/v1/auth", authRouter);

export const runApplication = () => {
  const port = process.env.APP_PORT || 5000;
  app.listen(port, () => {
    console.log("app is running...");
  });
};
