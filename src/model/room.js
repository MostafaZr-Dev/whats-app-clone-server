import mongoose from "mongoose";

import {messagesSchema} from "./messages.js";

const roomSchema = mongoose.Schema({
  name: String,
  messages: [messagesSchema],
});

export default mongoose.model("rooms", roomSchema);
