import mongoose from "mongoose";

export const MESSAGE_TYPE = ["text", "image", "audio"];

export const messagesSchema = mongoose.Schema({
  user: String,
  message: String,
  name: String,
  type: { type: String, enum: MESSAGE_TYPE, default: "text" },
  url: { type: String, default: null },
  timestamp: { type: Date, default: Date.now() },
});

export default mongoose.model("messages", messagesSchema);
