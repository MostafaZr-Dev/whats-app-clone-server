import mongoose from "mongoose";

const userSchema = mongoose.Schema({
  userName: String,
  email: String,
  password: { type: String, required: true },
});

export default mongoose.model("users", userSchema);
