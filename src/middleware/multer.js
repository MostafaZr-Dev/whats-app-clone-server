import multer from "multer";
import path from "path";
import fs from "fs";

const __dirname = path.resolve();
const uploadDir = path.join(__dirname, `/public/uploads`);

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    const { roomId } = req.params;

    const fileType = file.mimetype.split("/").shift();

    if (!fs.existsSync(`${uploadDir}/`)) {
      fs.mkdirSync(dir);
    }

    if (fileType === "audio") {
      if (!fs.existsSync(`${uploadDir}/voices/${roomId}`)) {
        fs.mkdirSync(`${uploadDir}/voices/${roomId}`);
      }

      cb(null, path.join(__dirname, `/public/uploads/voices/${roomId}`));
      return;
    }

    if (!fs.existsSync(`${uploadDir}/images/${roomId}`)) {
      fs.mkdirSync(`${uploadDir}/images/${roomId}`);
    }
    cb(null, path.join(__dirname, `/public/uploads/images/${roomId}`));
  },
  filename: function (req, file, cb) {
    const { roomId } = req.params;

    const fileType = file.mimetype.split("/").shift();
    const extentionFile = file.originalname.split(".").pop();

    if (fileType === "audio") {
      cb(null, `audio-${roomId}-${Date.now()}.${extentionFile}`);
      return;
    }

    cb(null, `image-${roomId}-${Date.now()}.${extentionFile}`);
  },
});

const uploadAudio = multer({ storage: storage });

export default uploadAudio;
